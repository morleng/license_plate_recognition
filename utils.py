import numpy as np


def thresholding(charList):
    topList = []
    for c in charList:
        topList.append(c[2][1])

        # print(topList)
    maxTop = max(topList)
    minTop = min(topList)

    threshold = (minTop + maxTop) / 2

    return threshold


def lineSegmentation(charList):
    upperLine = []
    bottomLine = []
    bottomLineIdx = []

    threshold = thresholding(charList)

    for k in charList:
        if k[2][1] < threshold:
            upperLine.append(k)
        else:
            bottomLine.append(k)
            bottomLineIdx.append(charList.index(k))

    return upperLine, bottomLine


def findProvince(data):
    length = len(data)
    res_list = [data[i][0] for i in range(length)]
    # print(res_list)
    max_len = (max(res_list, key=len))
    # print("province =>", max_len)

    # Check whether province is exist or not
    if len(max_len) > 1:
        return True
    else:
        return False


def getProvince(bottomLine):
    length = len(bottomLine)
    res_list = [bottomLine[i][0] for i in range(length)]
    max_len = (max(res_list))

    province = max(res_list)
    provinceIdx = res_list.index(max(res_list))

    return province, provinceIdx


def sort_label(alphabets):
    list_xmin = []
    list_classes = []

    for alphabet in alphabets:
        location = alphabet[2]
        label = alphabet[0]

        left = location[0]
        top = location[1]
        right = location[2]
        bottom = location[3]

        list_xmin.append(left)
        list_classes.append(label)

    # sort alphabets according to "x" axis
    sorted_lefts = np.argsort(list_xmin)
    list_classes = np.array(list_classes)
    sorted_labels = list_classes[sorted_lefts]

    return sorted_labels


def sortingLicensePlate(data):
    # Check whether a provice is exist or not
    found = findProvince(data)

    if found:
        upperLine, bottomLine = lineSegmentation(data)
        province, provinceIdx = getProvince(bottomLine)

        # print(len(bottomLine))
        # Cut a province off from Motorcycle plate
        if len(bottomLine) > 1:
            bottomLine.pop(provinceIdx)

        upperValue = []
        bottomValue = []

        upperSorted = sort_label(upperLine)
        bottomSorted = sort_label(bottomLine)

        # Check plate type (Car or Mortorcycle)
        if len(bottomLine) > 1:
            # Mortorcycle
            plate_string = np.append(upperSorted, bottomSorted)
        else:
            plate_string = upperSorted

        plate_string = ''.join(plate_string)
    else:
        sorted_labels = sort_label(data)

        plate_number = [sorted_label for sorted_label in sorted_labels if len(sorted_label) == 1]
        province = [sorted_label for sorted_label in sorted_labels if len(sorted_label) > 1]

        if len(province) > 1:
            province = province[0]
        else:
            province = None

        plate_string = ''.join(plate_number)

    return plate_string, province


def crop_lp(detections, image):
    cropped_images = []
    for detection in detections:
        location = detection[2]
        # replace negative values with 1
        new_location = [1 if i <= 0 else i for i in location]
        left = new_location[0]
        top = new_location[1]
        right = new_location[2]
        bottom = new_location[3]
        cropped_images.append(image[top:bottom, left:right, :])

    return cropped_images


def crop_image(detections, image):
    cropped_images = []
    for detection in detections:
        location = detection[2]
        # replace negative values with 1
        left = location[0]
        top = location[1]
        right = location[2]
        bottom = location[3]
        cropped_images.append(image[top:bottom, left:right, :])

    return cropped_images
