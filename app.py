from fastapi import FastAPI
import cv2
from pydantic import BaseModel
from typing import List
import uvicorn

import base64
import numpy as np
from ObjectDetection import OpencvYOLO
from configparser import ConfigParser


#Read config.ini file
config_object = ConfigParser()
config_object.read("config.ini")
OCR_CONFIG = config_object["OCR_CONFIG"]
# LICENSE_PLATE_CONFIG = config_object["LICENSE_PLATE_CONFIG"]


ocr_cfg = OCR_CONFIG["ocr_cfg"]
ocr_weight = OCR_CONFIG["ocr_weights"]
ocr_name = OCR_CONFIG["ocr_names"].encode('utf8')


ocr_detector = OpencvYOLO(cfg=ocr_cfg, weights=ocr_weight, objnames=ocr_name)

#Data validation
class Location(BaseModel):
    x: int
    y: int 
    w: int 
    h: int


class Object(BaseModel):
    label: str
    score: float
    location: Location


class Objects(BaseModel):
    __root__: List[Object]


class RecognizeImage(BaseModel):
    image: str


app = FastAPI(title="License plate recognition API")


@app.get('/')
async def hello_world():
    return {"Hello": "world!"}

numbers = "1, 2, 3, 4, 5, 6, 7, 8, 9, 0"
alphabets = "ก, ข, ค, ฆ, ง, จ, ฉ, ช, ฌ, ญ, ฎ, ฐ, ฒ, ณ, ด, ต, ถ, ท, ธ, น, บ, ป, ผ, พ, ฟ, ภ, ม, ย, ร, ล, ว, ศ, ษ, ส, ห, ฬ, อ, ฮ"
thai_porvinces = "กรุงเทพมหานคร, กาญจนบุรี, กาฬสินธุ์, กำแพงเพชร, ขอนแก่น, ฉะเชิงเทรา, ชลบุรี, ชัยนาท, ชัยภูมิ, เชียงราย, เชียงใหม่, ตราด, นครปฐม"
"นครราชสีมา, นครสวรรค์, บึงกาฬ, บุรีรัมย์, ปทุมธานี, ประจวบคีรีขันธ์, ปราจีนบุรี, พิจิตร, พิษณุโลก, เพชรบุรี, เพชรบูรณ์, ภูเก็ต, มหาสารคาม, มุกดาหาร, ร้อยเอ็ด" 
"ระยอง, ราชบุรี, ลพบุรี, ศรีสะเกษ, สงขลา, สตูล, สมุทรสาคร, สระบุรี, สุโขทัย, สุพรรณบุรี, สุราษฎร์ธานี, สุรินทร์, หนองคาย, พระนครศรีอยุธยา, อ่างทอง, อุดรธานี"
"อุบลราชธานี, สมุทรปราการ, นนทบุรี, อำนาจเจริญ, ระนอง, สิงห์บุรี, นครศรีธรรมราช, อุตรดิตถ์, สมุทรสงคราม, อุทัยธานี, แพร่, นครนายก, ยโสธร, พัทลุง, สกลนคร" 
"ตรัง, จันทบุรี, ลำปาง, เลย, ตาก, ลำพูน, น่าน, สระแก้ว, นครพนม, ปัตตานี, พังงา, ชัยนาถ, ยะลา, ชุมพร, แม่ฮ่องสอน, หนองบัวลำภู, นราธิวาส, กระบี่"
classes = numbers + alphabets + thai_porvinces

@app.post("/licensePlateRecognition/", response_model=Objects,
          summary="OCR API",
          description="Hello world!")
async def license_plate_recognition(base64_img: RecognizeImage):
    raw_image = base64_img.image
    trim_base64 = raw_image[raw_image.find(",") + 1:]
    if isBase64(trim_base64):
        processed_img = await preprocessing(base64_img.image)
        object_list = ocr_detector.getObject(processed_img, labelWant=classes,
                                                  drawBox=False)
        if len(object_list) >= 1:
            print(len(object_list))
            # object_list = vehicle_detector.getResult()
            # print(type(object_list))
            return object_list
        else:
            plate_list = [
                {
                    "label": "Object not found!",
                    "score": 0,
                    "location": {
                    "x": 0,
                    "y": 0,
                    "w": 0,
                    "h": 0
                    }
                }
            ]
            return plate_list
    else:
        return {"msg": "Invalid input image"}


# @app.post("/package/{priority}")
# async def make_package(priority: int, package: Package, value: bool):
#     return {"priority": priority, **package.dict(), "value": value}


async def preprocessing(base64_img):
    trim_base64 = base64_img[base64_img.find(",") + 1:]
    if isBase64(trim_base64):
        print("Converted to numpy ndarray")
        img_bytes = base64.b64decode(trim_base64)
        # print(img_bytes)
        img_array = np.frombuffer(img_bytes, dtype=np.uint8)  # img_array is one-dim Numpy array
        np_array = cv2.imdecode(img_array, flags=cv2.IMREAD_COLOR)

        try:
            rgb_img = cv2.cvtColor(np_array, cv2.COLOR_BGR2RGB)
        except ValueError:
            print("Could not convert empty array to RGB.")
        return rgb_img
    else:
        print("Can't convert to ndarray ")
        return False


def isBase64(sb):
    try:
        if isinstance(sb, str):
            # If there's any unicode here, an exception will be thrown and the function will return false
            sb_bytes = bytes(sb, 'ascii')
        elif isinstance(sb, bytes):
            sb_bytes = sb
        else:
            raise ValueError("Argument must be string or bytes")
        return base64.b64encode(base64.b64decode(sb_bytes)) == sb_bytes
    except Exception:
        return False


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=7200)